export declare type AlertType = "info" | "success" | "warning" | "error";
