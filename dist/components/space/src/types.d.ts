/** 间距尺寸 */
export declare type SizeType = "mini" | "small" | "normal" | "large" | number;
/** 间距方向 */
export declare type SpaceType = "vertical" | "horizontal";
