/** 按钮类型，普通、主要、线框、虚线、文本 */
export declare type ButtonType = "normal" | "primary" | "plain" | "dashed" | "text";
/** 按钮形状，矩形、圆滑 */
export declare type ButtonShape = "square" | "round" | "circle";
/** 按钮尺寸，迷你、小型、普通、大型 */
export declare type ButtonSize = "mini" | "small" | "normal" | "large";
/** 按钮状态，普通、主要、成功、警示、危险 */
export declare type ButtonStatus = "normal" | "primary" | "success" | "warning" | "danger";
