/** 链接状态，主要、成功、警示、危险 */
export declare type LinkStatus = "normal" | "primary" | "success" | "warning" | "danger";
