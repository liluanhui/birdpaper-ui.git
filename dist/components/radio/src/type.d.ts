/** 单选框组绑定值 */
export declare type RadioGroupValue = string | number;
/** 单选框类型 */
export declare type RadioType = "radio" | "button";
/** 单选框方向 */
export declare type DirectionType = "vertical" | "horizontal";
