# 更新日志

`birdpaper-ui` 遵循 Semver 语义化版本规范。

::: timeline 2023-10-12
<!--@include: ../change-log/2.1.2.md-->
:::
::: timeline 2023-10-10
<!--@include: ../change-log/2.1.1.md-->
:::
::: timeline 2023-10-08
<!--@include: ../change-log/2.1.0.md-->
:::
::: timeline 2023-08-30
<!--@include: ../change-log/2.0.3.md-->
:::
::: timeline 2023-08-20
<!--@include: ../change-log/2.0.2.md-->
:::
::: timeline 2023-08-12
<!--@include: ../change-log/2.0.0.md-->
:::